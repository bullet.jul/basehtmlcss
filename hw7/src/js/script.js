// 'use strict'

const initalState = {
    message: {
        loading: 'Loading',
        success: "Form was send. Thanks",
        error: 'Something went wrong',
        passWrong: "Password wrong"
    },
    color: {
        success: 'var(--green)',
        usual: 'black',
        error: 'red'
    }
}

const setState = (message, color) => {
    statusM.textContent =  initalState.message[message];
    statusM.style.color = initalState.color[color];
}


const form = document.querySelector('form'),
    inputs = document.querySelectorAll('input'),
    statusM = document.querySelector('.status'),
    passwordInput = document.querySelector('.password'),
    passwordInputConfirm = document.querySelector('.confpassword');


const request = async (url, data) => {
    setState('loading', 'usual');
    try {
        const response = await fetch(url, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: data
        });

        if (!response.ok) {
            throw new Error(`Form wasn't send ${response.status}`);
        }
        setState('success', 'success');    
            
        return await response.json(); 

    } catch (err) {
        setState('error', 'error');
        console.log(err)
    }
    finally{
        clearInputs();
        document.querySelector('.reg-checkbox').checked = false;
        setTimeout(() => {
            statusM.classList.remove('active'); 
    }, 2000);
    }
}

    const clearInputs = () => {
        inputs.forEach(item => item.value = '')
    };

//send form - data
    form.addEventListener('submit', (event) => {
        event.preventDefault();
        
        statusM.classList.add('active')
        
        if (passwordInput.value !== passwordInputConfirm.value) {
            setState('passWrong', 'error')
            setTimeout(() => {
                statusM.classList.remove('active');
            }, 1000);
            return;
        }

        const formData = new FormData(form); 
        const json = JSON.stringify(Object.fromEntries(formData.entries()));


        request('https://jsonplaceholder.typicode.com/posts', json)
        .then(res => {
            console.log('Form send')
        })


    })

